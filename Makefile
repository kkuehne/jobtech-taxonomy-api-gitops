#
# The Makefile handles deploying the Taxonomy API to OpenShift clusters
#
# There are targets that deploy to all three clusters; test, prod and standby. The targets
# first build the configuration from Dhall-files, and then deploys them using OpenShift command
# oc.
#
# E.g.
# make deploy-test - run Dhall compilation and oc apply on all test environments.
#
.ONESHELL:

# Suppress echoing of commands for these targets
.SILENT: volume-world-writable

# The project name in OpenShift
PROJECT = taxonomy-api-gitops

# Cluster URL's
TESTCLUSTER = "https://api.test.services.jtech.se:6443"
PRODCLUSTER = "https://api.prod.services.jtech.se:6443"
STANDBYCLUSTER = "https://api.standby.services.jtech.se:6443"

# Determine which cluster we are logged in to
CMD_CLUSTERURL = TERM=dumb LANG=C oc cluster-info | grep "running at" | sed 's|^.*running at ||'

# Usage: make set-api-image SHA=35fe8ad53bad8358f774cf557e1ce4c799238315
# Sets the deployment image for i1, u1 and frontend
DHALL_CONFIG = Dhall/Config.dhall
set-api-image:
	@if [ -z "$(SHA)" ]; then
		echo "**** error: env variable SHA not set"
		exit 1
	else
		sed -i.old "s/apiImageTest.*= \".*\"/apiImageTest : Text     = \"$(SHA)\"/g" $(DHALL_CONFIG)
	fi

promote-from-test:
	@API_SHA=$(shell grep "let.*apiImageTest" $(DHALL_CONFIG) | grep -o "\".*\"" | tr -d '"') && \
	VARNISH_SHA=$(shell grep "let.*varnishImageTest" $(DHALL_CONFIG) | grep -o "\".*\"" | tr -d '"') && \
	sed -i.old "s/apiImageProd.*= \".*\"/apiImageProd : Text     = \"$$API_SHA\"/g" $(DHALL_CONFIG) && \
	sed -i.old "s/varnishImageProd.*= \".*\"/varnishImageProd : Text = \"$$VARNISH_SHA\"/g" $(DHALL_CONFIG)

#
# Helper targets to login to OpenShift
#
login-test:
	oc login $(TESTCLUSTER) --password='' --username=''

login-prod:
	oc login $(PRODCLUSTER) --password='' --username=''

login-standby:
	oc login $(STANDBYCLUSTER) --password='' --username=''

#
# Build the configuration files
#
CONFIG_FILES = \
    ServiceAccounts.dhall \
    Backends.dhall \
    Deployments.dhall \
    Routes.dhall \
    Services.dhall

UTILS = \
    Config.dhall \
    Kubernetes.dhall \
    Prelude.dhall \
    OpenShift.dhall \
    Utils.dhall

test-%.yaml: $(addprefix Dhall/,${CONFIG_FILES} ${UTILS})
	CLUSTER='<Test|Prod|Standby>.Test' dhall-to-yaml --explain --file $(subst test-,Dhall/,$(patsubst %.yaml,%.dhall,$@)) --output $@

prod-%.yaml: $(addprefix Dhall/,${CONFIG_FILES} ${UTILS})
	CLUSTER='<Test|Prod|Standby>.Prod' dhall-to-yaml --explain --file $(subst prod-,Dhall/,$(patsubst %.yaml,%.dhall,$@)) --output $@

standby-%.yaml: $(addprefix Dhall/,${CONFIG_FILES} ${UTILS})
	CLUSTER='<Test|Prod|Standby>.Standby' dhall-to-yaml --explain --file $(subst standby-,Dhall/,$(patsubst %.yaml,%.dhall,$@)) --output $@

.PHONY: clean download-prodwrite
clean:
	rm -f test-*.yaml prod-*.yaml standby-*.yaml

build-test-config: $(addprefix test-,$(CONFIG_FILES:.dhall=.yaml))

build-prod-config: $(addprefix prod-,$(CONFIG_FILES:.dhall=.yaml))

build-standby-config: $(addprefix standby-,$(CONFIG_FILES:.dhall=.yaml))

#
# Varnish
#

restart-cache:
	@if [ -z "$(ENV)" ]; then
		echo "**** error: env variable ENV not set"
		echo "usage: make restart-cache ENV=i1"
		exit 1
	else
		@for pod in $(shell oc get pods --selector=app=taxonomy-app-$(ENV) | grep -oE varnish[^\ ]*); do
			echo "restarting $$pod"
			oc delete pod $$pod
		done
	fi

#
# Deploy the configuration files
#
check: gitcheck check-secrets

deploy-test: | check setup-test build-test-config deploy-gitops-sha
	@for yaml in $(addprefix test-,$(patsubst %.dhall,%.yaml,${CONFIG_FILES})); do
		echo "applying $$yaml"
		oc apply -f $$yaml
	done

deploy-prod: | check setup-prod build-prod-config deploy-gitops-sha
	@for yaml in $(addprefix prod-,$(patsubst %.dhall,%.yaml,${CONFIG_FILES})); do
		echo "applying $$yaml"
		oc apply -f $$yaml
	done

deploy-standby: | check setup-standby build-standby-config deploy-gitops-sha
	@for yaml in $(addprefix standby-,$(patsubst %.dhall,%.yaml,${CONFIG_FILES})); do
		echo "applying $$yaml"
		oc apply -f $$yaml
	done

#
# Bootstrap
# Commands that need to be run at least once and that may needs higher privileges. When the
# dependencies of these targets change the target needs to be manually run, it is not part
# of the normal deploy process.
#

bootstrap: boot-volumes

boot-volumes:
	# Datahike file backend
	oc apply -f database/claim.yaml
	oc apply -f database/create-volume.yaml

volume-world-writable:
	oc apply -f database/busybox-helper.yaml
	echo "sleep 30 seconds to wait for pod to start"
	sleep 30 # Wait for the pod to start
	oc get pod busybox-helper
	oc exec busybox-helper -- chmod 777 /mnt/datahike
	oc delete pod busybox-helper

#
# Datahike file backend targets
#

copy-file-database:
	@if [ -z "$(FOLDER)" ]; then
		echo "**** error: env variable FOLDER not set"
		echo "usage: make copy-file-database FOLDER=datahike-file-db"
		exit 1
	else
		oc apply -f database/busybox-helper.yaml
		echo "sleep 30 seconds to wait for pod to start"
		sleep 30 # Wait for the pod to start
		NAME=$(shell echo $(FOLDER) | sed 's/.*\///g')
		echo "copy database from $(FOLDER) to /mnt/datahike/$$NAME"
		oc cp $(FOLDER) busybox-helper:/mnt/datahike
		echo "change permissions in folder /mnt/datahike/$$NAME"
		oc exec pod/busybox-helper -- chmod -R 666 /mnt/datahike/$$NAME
		oc exec pod/busybox-helper -- chmod 777 /mnt/datahike/$$NAME
		oc delete pod busybox-helper
	fi

#### Helper targets - not to be run from command line directly #####

deploy-gitops-sha:
	@SHA=$(shell git rev-parse HEAD) && \
	DTYVERSION=$(shell dhall-to-yaml --version) && \
	DVERSION=$(shell dhall --version) && \
	sed "s/SHAVALUE/$$SHA/" gitops-sha.yaml | \
	sed "s/DTYVERSION/$$DTYVERSION/" | \
	sed "s/DVERSION/$$DVERSION/" | oc apply -f -

setup-test: check-project
	@CLUSTERURL=$(shell $(CMD_CLUSTERURL)) && \
	if [ "$$CLUSTERURL" != "$(TESTCLUSTER)" ]; then
		echo "**** error: not logged in to $(TESTCLUSTER), but logged in to $$CLUSTERURL"
		exit 1
	else
		echo "Operating on $$CLUSTERURL."
	fi
	kustomize build jobtech-taxonomy-api-deploy-secrets/test/ | oc apply -f -
	kustomize build jobtech-taxonomy-api-deploy-secrets/nexus/ | oc apply -f -
	oc apply -f tekton/build-pipeline.yaml
	oc apply -f tekton/mattermost.yaml
	oc apply -f tekton/pipeline-cleaner.yaml

setup-prod: check-project
	@CLUSTERURL=$(shell $(CMD_CLUSTERURL)) && \
	if [ "$$CLUSTERURL" != "$(PRODCLUSTER)" ]; then
		echo "**** error: not logged in to $(PRODCLUSTER), but logged in to $$CLUSTERURL"
		exit 1
	else
		echo "Operating on $$CLUSTERURL."
	fi
	kustomize build jobtech-taxonomy-api-deploy-secrets/prod/ | oc apply -f -
	kustomize build jobtech-taxonomy-api-deploy-secrets/nexus/ | oc apply -f -

setup-standby: check-project
	@CLUSTERURL=$(shell $(CMD_CLUSTERURL)) && \
	if [ "$$CLUSTERURL" != "$(STANDBYCLUSTER)" ]; then
		echo "**** error: not logged in to $(STANDBYCLUSTER), but logged in to $$CLUSTERURL"
		exit 1
	else
		echo "Operating on $$CLUSTERURL."
	fi
	kustomize build jobtech-taxonomy-api-deploy-secrets/prod/ | oc apply -f -
	kustomize build jobtech-taxonomy-api-deploy-secrets/nexus/ | oc apply -f -

check-project:
	@oc new-project $(PROJECT) || oc project $(PROJECT)
	@if [ "$$(oc project -q)" != "$(PROJECT)" ]; then
		echo "**** error: failed to switch to $(PROJECT)"
		exit 1
	fi

gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean"
		exit 1
	fi
	@if [ "$$(LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)" != "master" ]; then
		echo "**** error: not on the master branch"
		exit 1
	fi
	@git pull

check-secrets:
	@git submodule update --init --recursive
	@if [ ! -d "jobtech-taxonomy-api-deploy-secrets" ]; then
		echo "**** error: secrets are missing"
		exit 1
	fi



# Variables
prodwrite_db_file = build/prod_write.nippy

# Download the prod-write database
download-prodwrite-db:
	clj -X:task copy :src :datomic-prod-editor-config :dst '"$(prodwrite_db_file)"'

# Publish a new test database.
publish-test-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-test-config

# Copy to a backup repository. Pass the repository path using an environment variable.
# BACKUP_REPO_ROOT=/home/ubuntu/prog/jobtech-taxonomy-backup-files make copy-to-backup-repo
copy-to-backup-repo:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst '"$(BACKUP_REPO_ROOT)/prod-write/$(shell date +'%Y-%m-%d')/prod_write.nippy"'

# Publish a prodwrite copy database to frontend.
publish-frontend-test-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-test-frontend-config

publish-prod-read-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-prod-config

output-configs:
	clj -X:task output-configs

clean-db-files:
	rm -rf $(prodwrite_db_file)
