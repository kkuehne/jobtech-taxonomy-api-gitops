apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: build-and-deploy-dev
spec:
  workspaces:
  - name: shared-workspace
  params:
  - name: git-url
    type: string
    description: url of the git repo for the code of deployment
  - name: git-revision
    type: string
    description: revision/branch to be used from repo of the code for deployment
    default: "master"
  - name: image
    type: string
    description: image to be build from the code
  - name: ocs-ui-base-url
    type: string
    description: Base URL to OCS admin interface.
    default: "https://console-openshift-console.test.services.jtech.se"
  tasks:
  - name: fetch-repository
    taskRef:
      name: git-clone
      kind: ClusterTask
    workspaces:
    - name: output
      workspace: shared-workspace
    params:
    - name: url
      value: $(params.git-url)
    - name: subdirectory
      value: ""
    - name: deleteExisting
      value: "true"
    - name: revision
      value: $(params.git-revision)
  - name: build-image
    taskRef:
      name: buildah-v0-16-3
      kind: ClusterTask
    params:
    - name: IMAGE
      value: $(params.image)
    workspaces:
    - name: source
      workspace: shared-workspace
    runAfter:
    - fetch-repository
  - name: update-dev-env
    taskRef:
      name: update-dev-env-deployment
    params:
    - name: source
      value: $(params.git-url)
    - name: image
      value: $(params.image)
    runAfter:
    - build-image
  - name: run-kaocha-tests
    taskRef:
      name: run-kaocha-tests
    params:
    - name: source
      value: $(params.git-url)
    workspaces:
    - name: output
      workspace: shared-workspace
    runAfter:
    - update-dev-env
  finally:
    - name: notify-test-failure
      when:
        - input: $(tasks.status)
          operator: in
          values:
          - "Failed"
          - "None"
      taskRef:
        name: send-to-channel-mattermost
      params:
      - name: mattermost-api-url
        value: https://mattermost.jobtechdev.se/api/v4/posts
      - name: token-secret
        value: jobtech-taxonomy-api-secrets
      - name: channel
        value: 1pr6prepkjg7jgnoxn514xez9r
      - name: message
        value: "build-pipeline: The build or tests failed with commit $(params.git-url)/-/commit/$(params.git-revision). Check link for details:
          $(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
---
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: update-dev-env-deployment
spec:
  params:
  - name: source
  - name: image
  steps:
    - name: patch
      image: quay.io/openshift/origin-cli:latest
      command: ["/bin/bash", "-c"]
      args:
        - |-
          REPO=$(echo "$(params.source)" | sed 's/.*\///')
          DEP=$(if [ "$REPO" == "jobtech-taxonomy-api" ]; then echo "api"; else echo "varnish"; fi)
          oc patch deployment $DEP-dev --patch="{\"spec\":{\"template\":{\"spec\":{
            \"containers\":[{
              \"name\": \"$DEP-dev\",
              \"image\":\"$(params.image)\"
            }]
          }}}}"
---
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: run-kaocha-tests
  labels:
    operator.tekton.dev/disable-proxy: 'true'
spec:
  workspaces:
    - name: output
      description: The folder where the api repo is mounted
  params:
  - name: source
  steps:
    - name: run-kaocha-tests
      image: docker.io/clojure:openjdk-17-tools-deps
      workingDir: $(workspaces.output.path)
      command: ["/bin/bash", "-c"]
      args:
        - |-
          REPO=$(echo "$(params.source)" | sed 's/.*\///')
          echo "repository: $REPO"
          if [ "$REPO" == "jobtech-taxonomy-api" ] ; then
              apt update
              apt install -y git
              DATABASE_BACKEND=datahike \
                clj -M:test:kaocha
              if [ "$?" == "0" ] ; then
                  echo "The kaocha tests passed :)"
                  true
              else
                  echo "The kaocha tests failed!!!"
                  false
              fi
          else
              echo "No kaocha tests for $REPO"
              true
          fi
---
apiVersion: triggers.tekton.dev/v1alpha1
kind: TriggerTemplate
metadata:
  name: build-pipeline
spec:
  params:
  - name: git-url
    description: url of the git repo for the code of deployment
  - name: git-revision
    description: The git revision
  - name: git-repo-name
    description: The name of the deployment to be created / patched
  resourcetemplates:
  - apiVersion: tekton.dev/v1beta1
    kind: PipelineRun
    metadata:
      name: $(tt.params.git-repo-name)-$(uid)
    spec:
      serviceAccountName: pipeline
      pipelineRef:
        name: build-and-deploy-dev
      params:
      - name: git-url
        value: $(tt.params.git-url)
      - name: git-revision
        value: $(tt.params.git-revision)
      - name: image
        value: docker-images.jobtechdev.se/batfish/$(tt.params.git-repo-name):$(tt.params.git-revision)
      workspaces:
      - name: shared-workspace
        volumeClaimTemplate:
          spec:
            accessModes:
              - ReadWriteOnce
            resources:
              requests:
                storage: 500Mi
---
apiVersion: triggers.tekton.dev/v1alpha1
kind: TriggerBinding
metadata:
  name: build-pipeline
spec:
  params:
  - name: git-url
    value: $(body.project.web_url)
  - name: git-repo-name
    value: $(body.project.name)
  - name: git-revision
    value: $(body.checkout_sha)
---
apiVersion: triggers.tekton.dev/v1alpha1
kind: EventListener
metadata:
  name: build-pipeline
spec:
  serviceAccountName: pipeline
  triggers:
    - bindings:
      - ref: build-pipeline
      template:
        ref: build-pipeline
---
kind: Route
apiVersion: route.openshift.io/v1
metadata:
  name: el-build-pipeline
  labels:
    app.kubernetes.io/managed-by: EventListener
    app.kubernetes.io/part-of: Triggers
    eventlistener: build-pipeline
spec:
  to:
    kind: Service
    name: el-build-pipeline
    weight: 100
  port:
    targetPort: http-listener
  wildcardPolicy: None
