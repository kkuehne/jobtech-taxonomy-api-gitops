{-
  Import Kubernetes bindings only in one place
-}

--| Import Kubernetes 1.19 bindings
let Kubernetes =
      https://raw.githubusercontent.com/dhall-lang/dhall-kubernetes/master/1.19/package.dhall sha256:6774616f7d9dd3b3fc6ebde2f2efcafabb4a1bf1a68c0671571850c1d138861f

in Kubernetes
