{-
  Generates routes for the API and Varnish servers
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let map = Prelude.List.map

let default = Prelude.Optional.default

let OpenShift = ./OpenShift.dhall

let cfg = ./Config.dhall

let utils = ./Utils.dhall

let route
    : Text -> Text -> Natural -> Text -> OpenShift.Route.Type
    = \(app : Text) ->
      \(name : Text) ->
      \(port : Natural) ->
      \(host : Text) ->
        let port-str
            : Text
            = "${Natural/show port}-tcp"

        in  OpenShift.Route::{
            , metadata = OpenShift.ObjectMeta::{
              , name = Some name
              , labels = Some (toMap { app })
              }
            , spec = OpenShift.RouteSpec::{
              , host
              , port = Some OpenShift.RoutePort::{
                , targetPort = OpenShift.IntOrString.String port-str
                }
              , to = OpenShift.RouteTargetReference::{
                , kind = "Service"
                , name
                , weight = 100
                }
              }
            }

let addIngress
    : Text -> OpenShift.Route.Type -> OpenShift.Route.Type
    = \(name : Text) ->
      \(r : OpenShift.Route.Type) ->
        let lbls = r.metadata.labels

        let MapList = List { mapKey : Text, mapValue : Text }

        let newValue =
              default MapList ([] : MapList) lbls # toMap { type = name }

        in  r
          with metadata.labels = Some newValue

let addTls
    : OpenShift.TLSConfig.Type -> OpenShift.Route.Type -> OpenShift.Route.Type
    = \(tls : OpenShift.TLSConfig.Type) ->
      \(r : OpenShift.Route.Type) ->
        r
        with spec.tls = Some tls

let routeApi
    : cfg.Config -> OpenShift.Route.Type
    = \(c : cfg.Config) ->
        route (utils.appName c.name) (utils.apiName c.name) cfg.apiPort ""

let routeVarnish
    : cfg.Config -> OpenShift.Route.Type
    = \(c : cfg.Config) ->
        let tls =
              OpenShift.TLSConfig::{
              , termination = "edge"
              , insecureEdgeTerminationPolicy = Some "Allow"
              }

        let r =
              route
                (utils.appName c.name)
                (utils.varnishName c.name)
                cfg.varnishPort
                (utils.hostName c.name)

        in  addTls tls (addIngress "api-jobtechdev-se-route" r)

in  { apiVersion = "v1"
    , kind = "List"
    , items =
          map cfg.Config OpenShift.Route.Type routeApi cfg.config
        # map cfg.Config OpenShift.Route.Type routeVarnish cfg.config
    }
