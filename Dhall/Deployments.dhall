{-
  Generates deployments for the API and Varnish servers
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let map = Prelude.List.map

let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let utils = ./Utils.dhall

let livenessEp = "/v1/taxonomy/status/health"

let readinessEp = "/v1/taxonomy/status/ready"

--| Create a `kube.Probe.Type` object from a `port` and a `period` (seconds)
let probe
    : Integer -> Integer -> Text -> Text -> kube.Probe.Type
    = \(port : Integer) ->
      \(period : Integer) ->
      \(ep : Text) ->
      \(key : Text) ->
        kube.Probe::{
        , failureThreshold = Some +3
        , httpGet = Some kube.HTTPGetAction::{
          , httpHeaders = Some
            [ kube.HTTPHeader::{ name = "api-key", value = key } ]
          , path = Some ep
          , port = kube.IntOrString.Int port
          }
        , initialDelaySeconds = Some +30
        , periodSeconds = Some period
        , successThreshold = Some +1
        , timeoutSeconds = Some +5
        }

--| Create a `kube.Affinity.Type` object from an `app` name
let affinity
    : Text -> kube.Affinity.Type
    = \(app : Text) ->
        kube.Affinity::{
        , podAntiAffinity = Some kube.PodAntiAffinity::{
          , preferredDuringSchedulingIgnoredDuringExecution = Some
            [ kube.WeightedPodAffinityTerm::{
              , podAffinityTerm = kube.PodAffinityTerm::{
                , labelSelector = Some kube.LabelSelector::{
                  , matchLabels = Some (toMap { app })
                  }
                , topologyKey = "kubernetes.io/hostname"
                }
              , weight = +1
              }
            ]
          }
        }

--| Create a basic deployment object from an `app`, `name` and a list of `containers`
let deployment
    : Text ->
      Text ->
      Integer ->
      List kube.Container.Type ->
      Optional (List kube.Volume.Type) ->
        kube.Deployment.Type
    = \(app : Text) ->
      \(name : Text) ->
      \(replicas : Integer) ->
      \(containers : List kube.Container.Type) ->
      \(volumes : Optional (List kube.Volume.Type)) ->
        kube.Deployment::{
        , metadata = kube.ObjectMeta::{
          , name = Some name
          , labels = Some (toMap { app })
          }
        , spec = Some kube.DeploymentSpec::{
          , replicas = Some replicas
          , selector = kube.LabelSelector::{
            , matchLabels = Some (toMap { app })
            }
          , template = kube.PodTemplateSpec::{
            , metadata = Some kube.ObjectMeta::{
              , labels = Some (toMap { app })
              }
            , spec = Some kube.PodSpec::{
              , affinity = Some (affinity name)
              , containers
              , volumes
              }
            }
          }
        }

--| Create an API deployment object from an environment `Config`
let deploymentApi
    : cfg.Config -> kube.Deployment.Type
    = \(c : cfg.Config) ->
        let name
            : Text
            = utils.apiName c.name

        let port
            : Integer
            = Natural/toInteger cfg.apiPort

        let containers
            : List kube.Container.Type
            = [ kube.Container::{
                , envFrom = Some
                  [ kube.EnvFromSource::{
                    , secretRef = Some kube.SecretEnvSource::{
                      , name = Some "jobtech-taxonomy-api-secrets"
                      }
                    }
                  , kube.EnvFromSource::{
                    , configMapRef = Some kube.ConfigMapEnvSource::{
                      , name = Some c.dbConfigName
                      }
                    }
                  ]
                , name
                , image = Some "${cfg.apiImageUrl}:${c.imageApi}"
                , imagePullPolicy = Some "Always"
                , volumeMounts =
                    if    c.volumeMount
                    then  Some
                            [ kube.VolumeMount::{
                              , mountPath = "/mnt/datahike"
                              , name = "database"
                              }
                            ]
                    else  None (List kube.VolumeMount.Type)
                , ports = Some
                  [ kube.ContainerPort::{
                    , containerPort = port
                    , protocol = Some "TCP"
                    }
                  ]
                , livenessProbe = Some (probe port +30 livenessEp "liveness")
                , readinessProbe = Some (probe port +30 readinessEp "readiness")
                }
              ]

        let volumes
            : Optional (List kube.Volume.Type)
            = if    c.volumeMount
              then  Some
                      [ kube.Volume::{
                        , name = "database"
                        , persistentVolumeClaim = Some kube.PersistentVolumeClaimVolumeSource::{
                          , claimName = "datahike-file-db-claim"
                          }
                        }
                      ]
              else  None (List kube.Volume.Type)

        in  deployment
              (utils.appName c.name)
              name
              c.nbrApiPods
              containers
              volumes

--| Create a Varnish deployment object from an environment `Config`
let deploymentVarnish
    : cfg.Config -> kube.Deployment.Type
    = \(c : cfg.Config) ->
        let name
            : Text
            = utils.varnishName c.name

        let port
            : Integer
            = Natural/toInteger cfg.varnishPort

        let containers
            : List kube.Container.Type
            = [ kube.Container::{
                , env = Some
                  [ kube.EnvVar::{
                    , name = "TAXONOMYSERVICE"
                    , value = Some (utils.apiName c.name)
                    }
                  , kube.EnvVar::{
                    , name = "TAXONOMYSERVICEPORT"
                    , value = Some (Natural/show cfg.apiPort)
                    }
                  ]
                , name
                , image = Some "${cfg.varnishImageUrl}:${c.imageVarnish}"
                , imagePullPolicy = Some "Always"
                , ports = Some
                  [ kube.ContainerPort::{
                    , containerPort = port
                    , protocol = Some "TCP"
                    }
                  ]
                , livenessProbe = Some (probe port +30 livenessEp "liveness")
                , readinessProbe = Some (probe port +10 readinessEp "readiness")
                }
              ]

        in  deployment
              (utils.appName c.name)
              name
              c.nbrVarnishPods
              containers
              (None (List kube.Volume.Type))

--| Generate deployments for all environments for the API and Varnish
in  { apiVersion = "v1"
    , kind = "List"
    , items =
          map cfg.Config kube.Deployment.Type deploymentApi cfg.config
        # map cfg.Config kube.Deployment.Type deploymentVarnish cfg.config
    }
