{-
  The configuration file for the Taxonomy API environments
-}

--| Do not format these 4 lines of SHA values, the Makefile depends on the formatting for now
let apiImageTest : Text     = "c9fb04f6ecb35be60db3be74d3e164a71010295f"
let apiImageProd : Text     = "c9fb04f6ecb35be60db3be74d3e164a71010295f"
let varnishImageTest : Text = "f2d4d7396b14577bda362091ae29c4e208fed78d"
let varnishImageProd : Text = "f2d4d7396b14577bda362091ae29c4e208fed78d"

--| Import the Prelude
let Prelude = ./Prelude.dhall

--| Set environment variable CLUSTER to select environment config
--  See Makefile for details.
let Cluster = < Test | Prod | Standby >

let optCluster
    : Optional Cluster
    = Some env:CLUSTER ? None Cluster

let cluster
    : Cluster
    = Prelude.Optional.default Cluster Cluster.Test optCluster

let nexusUrl
    : Text
    = "docker-images.jobtechdev.se/batfish"

--| The base URL to images for the API server
let apiImageUrl
    : Text
    = "${nexusUrl}/jobtech-taxonomy-api"

--| The base URL to images for the Varnish server
let varnishImageUrl
    : Text
    = "${nexusUrl}/openshift-varnish"

--| The base name for the apps
let appBaseName
    : Text
    = "taxonomy-app"

--| The base name for the API
let baseName
    : Text
    = "api"

--| The port used by the API server
let apiPort
    : Natural
    = 3000

--| The port used by the Varnish server
let varnishPort
    : Natural
    = 8083

--| A `Config` type used to hold a configuration for an environment, e.g. the U1 environment
let Config
    : Type
    = { name : Text
      , imageApi : Text
      , nbrApiPods : Integer
      , imageVarnish : Text
      , nbrVarnishPods : Integer
      , dbConfigName : Text
      , volumeMount : Bool
      }

--| Create an environment `Config` from a `name`, `imageApi` (SHA-value) and a `imageVarnish` (SHA-value)
let makeConfig
    : Text -> Text -> Integer -> Text -> Integer -> Text -> Bool -> Config
    = \(name : Text) ->
      \(imageApi : Text) ->
      \(nbrApiPods : Integer) ->
      \(imageVarnish : Text) ->
      \(nbrVarnishPods : Integer) ->
      \(dbConfigName : Text) ->
      \(volumeMount : Bool) ->
        let config
            : Config
            = { name
              , imageApi
              , nbrApiPods
              , imageVarnish
              , nbrVarnishPods
              , dbConfigName
              , volumeMount
              }

        in  config

--| Create a list of environment `Config`s
let config
    : List Config
    = merge
        { Test =
            [ makeConfig "frontend" apiImageTest +1 varnishImageTest +1 "datomic-test-frontend-config" False
	    -- Temporary env called 'auto' to test a redesigned autocomplete
            , makeConfig "auto" "700086eae65a66b24952d475897fd687fef7a504" +1 varnishImageTest +1 "datomic-test-config" False
            , makeConfig "dev" apiImageTest +1 varnishImageTest +1 "datomic-test-config" False
            , makeConfig "u1" apiImageTest +1 varnishImageTest +1 "datomic-test-config" False
            , makeConfig "i1" apiImageTest +1 varnishImageTest +1 "datomic-test-config" False
            , makeConfig "t1" apiImageProd +1 varnishImageProd +1 "datomic-test-config" False
            , makeConfig "t2" apiImageProd +5 varnishImageProd +3 "datomic-test-config" False
            , makeConfig "d-i1" apiImageTest +1 varnishImageTest +1 "datahike-test-file-config" True
          ]
        , Prod =
            [ makeConfig "prod" apiImageProd +5 varnishImageProd +3 "datomic-prod-config" False
            , makeConfig "editor-prod" apiImageProd +1 varnishImageProd +1 "datomic-prod-editor-config" False
          ]
        , Standby =
          [ makeConfig
              "prod"
                "be61bcf202a71c5cd77a600c9e390f61dab39436" +5 -- Run a Datahike image in Standby
                varnishImageTest +3
              "datahike-config"
              False
          ]
        }
        cluster

--| Export necessary configuration types and constants
in  { Config
    , config
    , apiPort
    , varnishPort
    , appBaseName
    , baseName
    , apiImageUrl
    , varnishImageUrl
    , cluster
    }
