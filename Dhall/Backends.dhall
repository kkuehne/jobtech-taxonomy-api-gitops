{-
  Configure the database backends for the Taxonomy API Server
-}
let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let createDatomicConfig
    : Text -> Text -> Text -> Text -> Text -> kube.ConfigMap.Type
    = \(configName : Text) ->
      \(region : Text) ->
      \(system : Text) ->
      \(endpoint : Text) ->
      \(dbName : Text) ->
        kube.ConfigMap::{
        , metadata = kube.ObjectMeta::{ name = Some configName }
        , data = Some
            ( toMap
                { DATOMIC_CFG__REGION = region
                , DATOMIC_CFG__SYSTEM = system
                , DATOMIC_CFG__ENDPOINT = endpoint
                , datomic_name = dbName
                }
            )
        }

let datomicRegion
    : Text
    = "eu-central-1"

let datomicTestSystem
    : Text
    = "tax-test-v4"

let datomicProdSystem
    : Text
    = "tax-prod-v4"

let datomicTestEndpoint
    : Text
    = "http://tax-c-loadb-i0orbaqklmiy-22feddff72f82e2d.elb.eu-central-1.amazonaws.com:8182"

let datomicProdEndpoint
    : Text
    = "http://tax-c-loadb-2ujixczkeo0e-4baa81991e5ad6eb.elb.eu-central-1.amazonaws.com:8182"

let datomicTestConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-test-config"
        datomicRegion
        datomicTestSystem
        datomicTestEndpoint
        "jobtech-taxonomy-dev-2022-01-27-11-27-06"

let datomicTestFrontendConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-test-frontend-config"
        datomicRegion
        datomicTestSystem
        datomicTestEndpoint
        "jobtech-taxonomy-frontend-2022-01-27-17-47-19"

let datomicProdConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-prod-config"
        datomicRegion
        datomicProdSystem
        datomicProdEndpoint
        "jobtech-taxonomy-prod-2022-01-27-10-49-08"

let datomicProdEditorConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-prod-editor-config"
        datomicRegion
        datomicProdSystem
        datomicProdEndpoint
        "jobtech-taxonomy-prod-write-2022-01-27-08-34-01"

let createDatahikeFileConfig
    : Text -> Text -> kube.ConfigMap.Type
    = \(configName : Text) ->
      \(path : Text) ->
        kube.ConfigMap::{
        , metadata = kube.ObjectMeta::{ name = Some configName }
        , data = Some
            ( toMap
                { DATAHIKE_CFG__STORE__BACKEND = ":file"
                , DATAHIKE_CFG__STORE__PATH = path
                , DATABASE_BACKEND = "datahike"
                }
            )
        }

let datahikeTestImageConfig
    : kube.ConfigMap.Type
    = createDatahikeFileConfig "datahike-config" "/tmp"

let datahikeTestFileConfig
    : kube.ConfigMap.Type
    = createDatahikeFileConfig "datahike-test-file-config" "/mnt/datahike/datahike-file-db"

in  { apiVersion = "v1"
    , kind = "List"
    , items =
        merge
          { Test =
            [ datomicTestConfig, datomicTestFrontendConfig, datahikeTestFileConfig ]
          , Prod = [ datomicProdConfig, datomicProdEditorConfig ]
          , Standby = [ datahikeTestImageConfig ]
          }
          cfg.cluster
    }
