{-
  Generates services for the API and Varnish servers
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let map = Prelude.List.map

let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let utils = ./Utils.dhall

--| Create a basic service object from an `app`, `name` and a `port`
let service
    : Text -> Text -> Natural -> kube.Service.Type
    = \(app : Text) ->
      \(name : Text) ->
      \(port : Natural) ->
        kube.Service::{
        , metadata = kube.ObjectMeta::{
          , name = Some name
          , labels = Some (toMap { app })
          }
        , spec = Some kube.ServiceSpec::{
          , selector = Some (toMap { app })
          , type = Some "NodePort"
          , ports = Some
            [ kube.ServicePort::{
              , name = Some "${Natural/show port}-tcp"
              , targetPort = Some
                  (kube.IntOrString.Int (Natural/toInteger port))
              , port = Natural/toInteger port
              , protocol = Some "TCP"
              }
            ]
          }
        }

--| Create an API service object from an environment `Config`
let serviceApi
    : cfg.Config -> kube.Service.Type
    = \(c : cfg.Config) ->
        service (utils.appName c.name) (utils.apiName c.name) cfg.apiPort

--| Create a Varnish service object from an environment `Config`
let serviceVarnish
    : cfg.Config -> kube.Service.Type
    = \(c : cfg.Config) ->
        service
          (utils.appName c.name)
          (utils.varnishName c.name)
          cfg.varnishPort

--| Generate services for all environments for the API and Varnish
in  { apiVersion = "v1"
    , kind = "List"
    , items =
          map cfg.Config kube.Service.Type serviceApi cfg.config
        # map cfg.Config kube.Service.Type serviceVarnish cfg.config
    }
