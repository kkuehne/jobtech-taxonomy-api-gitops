## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Database migrations](#database-migrations)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)

## About The Project

A take on a declarative build and deploy setup for the [Jobtech Taxonomy API](https://gitlab.com/team-batfish/backend/jobtech-taxonomy-api).

It is built for OpenShift 4.7.

### Built With

* [Dhall](https://dhall-lang.org/)
* [oc - the OpenShift command line](https://github.com/openshift/oc)
* [Kustomize](https://github.com/kubernetes-sigs/kustomize)

### Why Dhall?
Dhall comes with some advantages compared to the previous kustomize solution:
- Less configuration/code duplication
- Uses the power of (total) functional programming
- The type system helps with writing correct configurations, during upgrades or refactoring

Bindings to Kubernetes is generated from the OpenAPI specification, so the compiler throughs a type error if you are not generating a sound configuration. Unfortunately the compiler can not detect all kinds of broken configurations, such as interdependencies between Kubernetes objects that can't be modelled in the type system.

### Gitops Workflow and Architecture

1. Make a commit in an API repo and push to GitLab. GitLab will send a webhook to OpenShift that will trigger a build of the commit. The image will be sent to Nexus.
2. Login to the cluster to deploy to (make login-(test|prod|standby)).
3. Push a deployment to the cluster (make deploy-(test|prod|standby)). The Makefile will trigger a build of the configuration using Dhall. The configuration is pushed to the OpenShift cluster (oc apply). OpenShift will spin up new envinronments (if any deployemnts have been changed), and these new deployments will get their images from Nexus.

![alt text](resources/gitops-flow.svg "Gitops workflow")

## Getting Started

### Prerequisites

Install the `dhall`, `dhall-json`, `oc` and `kustomize` tools. `kustomize` is currently only used by the test cluster.

### Installation

Clone this repo.

Then get the submodule: `git submodule update --init --recursive`

That submodule contains a root kustomization.yaml with the following content:
```
generatorOptions:
 disableNameSuffixHash: true

secretGenerator:
- name: jobtech-taxonomy-api-secrets
  envs:
  - secrets/api-secrets-test.txt
  - secrets/aws-secrets-test.txt
```

## Bootstrap

There is a bootstrap target used to setup infrastructure that requires more priviledges than a normal user has.
These are the things that are setup:
- Volumes for Datahike file backend, see [Persistance](https://gitlab.com/arbetsformedlingen/devops/calamari-documentation/-/blob/master/peristance.md)
for more information about creating a filesystem in AWS. This is required before creating the volume.

## Usage

Begin any session by logging in to the correct OpenShift cluster.
```
make login-test
```

Deploys are made using `make`.
```
make deploy-test
```

## Database migrations

To publish a new *test* database from the *production write* database, first call
```
make download-prodwrite-db
```
which will download the prodwrite database in the form of a sequence of datoms and save it to `build/prod_write.nippy`.

It may be a good idea to take a backup of the database to the [jobtech-taxonomy-backup-files](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-backup-files) repository. Copy the file there by calling

```
BACKUP_REPO_ROOT=/home/ubuntu/prog/jobtech-taxonomy-backup-files make copy-to-backup-repo
```
changing the path to the repository as needed. You will manually have to add and commit the copied file to the repository.

Finally, to publish a new database, call
```
make publish-test-db
```

## Roadmap

See the [open issues](https://gitlab.com/team-batfish/backend/jobtech-taxonomy-api-gitops/-/issues) for a list of proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


## License

Distributed under the GNU GPL v3 License. See `LICENSE` for more information.


## Contact

[jobtechdev@arbetsformedlingen.se](mailto:jobtechdev@arbetsformedlingen.se)

Project Link: [https://gitlab.com/team-batfish/backend/jobtech-taxonomy-api-gitops](https://gitlab.com/team-batfish/backend/jobtech-taxonomy-api-gitops)
